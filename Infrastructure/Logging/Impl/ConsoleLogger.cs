﻿using JetBrains.Annotations;
using System;
using System.Text;

namespace Infrastructure.Logging.Impl
{
    public class ConsoleLogger : ILogger
    {
        #region Public Methods

        public void Error([NotNull] string message)
        {
            Console.WriteLine(AddAdditionalInfo(message, true));
        }

        public void Info([NotNull] string message)
        {
            Console.WriteLine(AddAdditionalInfo(message, false));
        }

        #endregion Public Methods

        #region Private Methods

        private string AddAdditionalInfo(string message, bool error)
        {
            var sb = new StringBuilder();
            var time = DateTime.Now.ToShortTimeString();
            var type = error ? "ERROR" : "INFO ";
            return $"{Surround(type)}{Surround(time)}: {message}";
        }

        private string Surround(string sb)
        {
            var surounded = new StringBuilder();
            surounded.Append("[").Append(sb).Append("]");
            return surounded.ToString();
        }

        #endregion Private Methods
    }
}