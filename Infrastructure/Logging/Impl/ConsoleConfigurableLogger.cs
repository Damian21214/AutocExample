﻿using System;
using System.Text;

namespace Infrastructure.Logging.Impl
{
    public class ConsoleConfigurableLogger : ILogger
    {
        #region Public Constructors

        public ConsoleConfigurableLogger(char separator, string prefix)
        {
            this._separator = separator;
            this._prefix = prefix;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Error(string message)
        {
            Console.WriteLine(AddAdditionalInfo(message, true));
        }

        public void Info(string message)
        {
            Console.WriteLine(AddAdditionalInfo(message, false));
        }

        #endregion Public Methods

        #region Private Fields

        private readonly string _prefix;
        private readonly char _separator;

        #endregion Private Fields

        #region Private Methods

        private string AddAdditionalInfo(string message, bool error)
        {
            var sb = new StringBuilder();
            var time = DateTime.Now.ToShortTimeString();
            var type = error ? "ERROR" : "INFO ";
            return $"{_prefix} {Surround(type)}{Surround(time)}: {message}";
        }

        private string Surround(string sb)
        {
            var surounded = new StringBuilder();
            surounded.Append(this._separator).Append(sb).Append(this._separator);
            return surounded.ToString();
        }

        #endregion Private Methods
    }
}