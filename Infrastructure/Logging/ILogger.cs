﻿namespace Infrastructure.Logging
{
    public interface ILogger
    {
        #region Public Methods

        void Error(string message);

        void Info(string message);

        #endregion Public Methods
    }
}