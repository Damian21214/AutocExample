﻿namespace Infrastructure.Configuration
{
    public interface IAppConfig
    {
        char LoggerSeparator { get; }
    }
}