﻿using System.Configuration;

namespace Infrastructure.Configuration.Impl
{
    public class ApplicationConfigurationXml : IAppConfig
    {
        public char LoggerSeparator 
        {
            get
            {
                var configValue = ConfigurationManager.AppSettings["LoggerSeparator"];
                return string.IsNullOrWhiteSpace(configValue)
                    ? '|'
                    : configValue[0];
            }
        }
    }
}