﻿using Domain.Model;
using Infrastructure.Logging.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Impl.NH;

namespace Services.Impl.Tests
{
    [TestClass]
    public class PersonServiceTests
    {
        private readonly NHSessionManager sessionManager;

        public PersonServiceTests()
        {
            NHConfig config = new NHConfig();

            var factory = config.Configure();

            sessionManager = new NHSessionManager(factory);
        }

        [TestMethod]
        public void TestAddPerson()
        {
            var service = new PersonService(sessionManager, new ConsoleLogger());

            Person person = new Person();
            person.FirstName = "FirstName";
            person.LastName = "LastName";

            var id = service.Add(person);

            Assert.IsNotNull(id);
        }
    }
}