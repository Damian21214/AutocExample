﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Impl.NH;

namespace Services.Impl.Tests
{
    [TestClass]
    public class NHConfigTests
    {
        [TestMethod]
        public void TestNHConfig()
        {
            NHConfig config = new NHConfig();

            var factory = config.Configure();

            Assert.IsNotNull(config);
            Assert.IsNotNull(factory);
        }

        [TestMethod]
        public void TestNHSession()
        {
            NHConfig config = new NHConfig();

            var factory = config.Configure();

            NHSessionManager sessionManager = new NHSessionManager(factory);

            var session = sessionManager.GetSession();

            Assert.IsNotNull(session);
        }
    }
}