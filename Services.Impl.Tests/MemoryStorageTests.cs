﻿using Domain.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Services.Impl.Mem.Tests
{
    [TestClass()]
    public class MemoryStorageTests
    {
        [TestMethod()]
        public void AddTest()
        {
            var ms = new MemoryStorage();

            var person = new Person()
            {
                LastName = "Damian"
            };

            var address = ms.Add(new Address()
            {
                Person = person
            });
            Assert.AreEqual(1, address.Id);
            Assert.AreEqual(1, address.Person.Id);
        }

        [TestMethod()]
        public void FindByPersonTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FindAllTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FindByLastNameTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void AddTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DeleteTest1()
        {
            Assert.Fail();
        }
    }
}