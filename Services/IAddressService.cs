﻿using Domain.Model;
using System.Collections.Generic;

namespace Services
{
    public interface IAddressService
    {
        #region Public Methods

        Address Add(Address address);

        bool Delete(Address address);

        IList<Address> FindByPerson(Person person);

        Address Get(int id);

        #endregion Public Methods
    }
}