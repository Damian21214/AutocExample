﻿using Domain.Model;
using System.Collections.Generic;

namespace Services
{
    public interface IPersonService
    {
        #region Public Methods

        Person Add(Person person);

        bool Delete(Person person);

        IList<Person> FindAll();

        IList<Person> FindByLastName(string pattern);

        Person Get(int id);
        Person Update(Person person);

        #endregion Public Methods
    }
}