﻿using Domain.Model;
using Infrastructure.Logging;
using NHibernate;
using System;
using System.Collections.Generic;

namespace Services.Impl.NH
{
    public class AddressService : BaseService, IAddressService
    {
        #region Public Constructors

        public AddressService(NHSessionManager manager, ILogger logger) : base(manager, logger)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public Address Add(Address address)
        {
            using (var session = GetSession())
            {
                try
                {
                    session.Save(address);
                    session.Flush();
                    session.Refresh(address);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    address = null;
                }
            }

            return address;
        }

        public bool Delete(Address address)
        {
            bool result = false;
            using (var session = GetSession())
            {
                try
                {
                    session.Delete(address);
                    session.Flush();
                    result = true;
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return result;
        }

        public IList<Address> FindByPerson(Person person)
        {
            IList<Address> addresses = null;

            using (var session = GetSession())
            {
                var query = session
                    .QueryOver<Address>()
                    .Where(a => a.Person == person)
                    .OrderBy(a => a.City).Asc;

                try
                {
                    addresses = query.List();
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return addresses;
        }

        public Address Get(int id)
        {
            Address address = null;
            using (var session = GetSession())
            {
                try
                {
                    session.Get<Address>(id);
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return address;
        }

        #endregion Public Methods
    }
}