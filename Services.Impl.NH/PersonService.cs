﻿using Domain.Model;
using Infrastructure.Logging;
using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace Services.Impl.NH
{
    public class PersonService : BaseService, IPersonService
    {
        #region Public Constructors

        public PersonService(NHSessionManager manager, ILogger logger) : base(manager, logger)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public Person Add(Person person)
        {
            using (var session = GetSession())
            {
                try
                {
                    session.Save(person);
                    session.Flush();
                    session.Refresh(person);
                }
                catch (HibernateException ex)
                {
                    person = null;
                    Logger.Error(ex.Message);
                }
            }

            return person;
        }

        public bool Delete(Person person)
        {
            bool result = false;

            using (var session = GetSession())
            {
                try
                {
                    session.Delete(person);
                    session.Flush();
                    result = true;
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return result;
        }

        public IList<Person> FindAll()
        {
            IList<Person> personsList = null;

            using (var session = GetSession())
            {
                var query = session.QueryOver<Person>();

                try
                {
                    personsList = query.List();
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return personsList;
        }

        public IList<Person> FindByLastName(string pattern)
        {
            IList<Person> personsList = null;

            using (var session = GetSession())
            {
                var query = session
                    .QueryOver<Person>()
                    .WhereRestrictionOn(p => p.LastName)
                    .IsInsensitiveLike(pattern, MatchMode.Anywhere)
                    .OrderBy(p => p.LastName).Asc
                    .ThenBy(p => p.FirstName).Asc;

                try
                {
                    personsList = query.List();
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
                return personsList;
            }
        }

        public Person Get(int id)
        {
            Person person = null;

            using (var session = GetSession())
            {
                try
                {
                    person = session.Get<Person>(id);
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                }
            }

            return person;
        }

        public Person Update(Person person)
        {
            using (var session = GetSession())
            {
                try
                {
                    session.Update(person);
                    session.Flush();
                }
                catch (HibernateException ex)
                {
                    Logger.Error(ex.Message);
                    person = null;
                }
            }

            return person;
        }

        #endregion Public Methods
    }
}