﻿using NHibernate;

namespace Services.Impl.NH
{
    public class NHSessionManager
    {
        #region Public Constructors

        public NHSessionManager(ISessionFactory factory)
        {
            this._sessionFactory = factory;
        }

        #endregion Public Constructors

        #region Public Methods

        public ISession GetSession()
        {
            var session = this._sessionFactory.OpenSession();
            return session;
        }

        #endregion Public Methods

        #region Private Fields

        private readonly ISessionFactory _sessionFactory;

        #endregion Private Fields
    }
}