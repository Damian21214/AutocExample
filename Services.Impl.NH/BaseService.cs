﻿using Infrastructure.Logging;
using NHibernate;

namespace Services.Impl.NH
{
    public class BaseService
    {
        #region Public Constructors

        public BaseService(NHSessionManager sessionManager, ILogger logger)
        {
            this._nhSessionManager = sessionManager;
            this._logger = logger;
        }

        #endregion Public Constructors

        #region Protected Properties

        protected ILogger Logger => _logger;

        #endregion Protected Properties

        #region Protected Methods

        protected ISession GetSession()
        {
            return _nhSessionManager.GetSession();
        }

        #endregion Protected Methods

        #region Private Fields

        private readonly ILogger _logger;
        private readonly NHSessionManager _nhSessionManager;

        #endregion Private Fields
    }
}