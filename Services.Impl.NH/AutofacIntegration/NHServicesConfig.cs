﻿using Autofac;

namespace Services.Impl.NH.ContainerConfig
{
    public class NHServicesConfig : Module
    {
        #region Protected Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NHConfig>().SingleInstance();
            builder.Register(c =>
            {
                var nhconfig = c.Resolve<NHConfig>();
                var factory = nhconfig.Configure();
                return factory;
            }
            ).SingleInstance();
            builder.RegisterType<NHSessionManager>().AsSelf();
            builder.RegisterType<BaseService>().AsSelf();

            builder.RegisterType<PersonService>().As<IPersonService>();
            builder.RegisterType<PersonService>().Named<IPersonService>("nhPersonService");

            builder.RegisterType<AddressService>().As<IAddressService>();
            builder.RegisterType<AddressService>().Named<IAddressService>("nhAddressService");
        }

        #endregion Protected Methods
    }
}