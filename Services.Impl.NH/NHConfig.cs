﻿using NHibernate;
using NHibernate.Cfg;
using System.Reflection;

namespace Services.Impl.NH
{
    public class NHConfig
    {
        #region Public Methods

        public ISessionFactory Configure()
        {
            // Initialize
            var cfg = new Configuration();
            cfg.Configure();

            // Add class mappings to configuration object
            Assembly thisAssembly = typeof(Services.Impl.NH.NHConfig).Assembly;
            cfg.AddAssembly(thisAssembly);

            // Create session factory from configuration object
            return cfg.BuildSessionFactory();
        }

        #endregion Public Methods
    }
}