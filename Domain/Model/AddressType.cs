﻿namespace Domain.Model
{
    public enum AddressType
    {
        HOME,
        WORK,
        OTHER
    }
}