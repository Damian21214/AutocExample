﻿using JetBrains.Annotations;

namespace Domain.Model
{
    public class Address
    {
        #region Public Properties

        public virtual string City { get; set; }
        public virtual string Country { get; set; }
        public virtual int Id { get; set; }

        [NotNull]
        public virtual Person Person { get; set; }

        public virtual string Street { get; set; }

        public virtual string StreetNo { get; set; }

        [NotNull]
        public virtual AddressType Type { get; set; }

        #endregion Public Properties
    }
}