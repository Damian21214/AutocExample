﻿using JetBrains.Annotations;
using System.Collections.Generic;

namespace Domain.Model
{
    public class Person
    {
        #region Public Properties

        public virtual IList<Address> Addresses { get; set; }
        public virtual string FirstName { get; set; }
        public virtual int Id { get; set; }
        [NotNull]
        public virtual string LastName { get; set; }

        public virtual string SecondName { get; set; }
        public virtual string Title { get; set; }

        #endregion Public Properties

        #region Public Methods

        public override string ToString()
        {
            return $"ID: {Id}, LastName: {LastName}, FirstName: {FirstName}";
        }

        #endregion Public Methods
    }
}