﻿using Autofac;

namespace Services.Impl.Mem.AutofacIntegration
{
    public class MemServicesConfig : Module
    {
        #region Protected Methods

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MemoryStorage>().As<IAddressService>().SingleInstance();
            builder.RegisterType<MemoryStorage>().Named<IAddressService>("memAddressService").SingleInstance();

            builder.RegisterType<MemoryStorage>().As<IPersonService>().SingleInstance();
            builder.RegisterType<MemoryStorage>().Named<IPersonService>("memPersonService").SingleInstance();
        }

        #endregion Protected Methods
    }
}