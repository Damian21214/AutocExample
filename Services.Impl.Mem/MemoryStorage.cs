﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Services.Impl.Mem
{
    public partial class MemoryStorage : IPersonService, IAddressService
    {
        #region Public Methods

        public Address Add(Address address)
        {
            var person = address.Person;

            PersonsRow personRow = null;

            if (person != null)
            {
                personRow = this.Persons.SingleOrDefault(p => p.Id == person.Id) ??
                            this.Persons.AddPersonsRow(address.Person.FirstName,
                                                        address.Person.SecondName,
                                                        address.Person.LastName,
                                                        address.Person.Title);
            }

            var addredRow = this.Addresses.AddAddressesRow((int)address.Type, address.Street, address.StreetNo, address.City,
                address.Country, personRow);

            address.Id = addredRow.Id;
            address.Person.Id = personRow.Id;

            return address;
        }

        public Person Add(Person person)
        {
            if (this.Persons.SingleOrDefault(p => p.Id == person.Id) == null)
            {
                var row = this.Persons.AddPersonsRow(person.FirstName,
                    person.SecondName,
                    person.LastName,
                    person.Title);

                person.Id = row.Id;

                if (person.Addresses != null)
                {
                    foreach (var address in person.Addresses)
                    {
                        var addressRow = this.Add(address);
                        address.Id = addressRow.Id;
                    }
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }

            return person;
        }

        public bool Delete(Address address)
        {
            var row = this.Addresses.SingleOrDefault(a => a.Id == address.Id);
            var result = false;
            if (row != null)
            {
                this.Addresses.RemoveAddressesRow(row);
                result = true;
            }
            return result;
        }

        public bool Delete(Person person)
        {
            throw new System.NotImplementedException();
        }

        public IList<Person> FindAll() => this.Persons
            .Select(RowToPerson)
            .ToList();

        public IList<Person> FindByLastName(string pattern) => this.Persons
            .Where(r => r.LastName.ToUpper().IndexOf(pattern.ToUpper(), StringComparison.Ordinal) > 0)
            .Select(RowToPerson)
            .ToList();

        public IList<Address> FindByPerson(Person person) => this.Addresses
            .Where(a => a.PersonId == person.Id)
            .Select(RowToAddress)
            .ToList();

        Address IAddressService.Get(int id) => RowToAddress(this.Addresses.SingleOrDefault(a => a.Id == id));

        Person IPersonService.Get(int id) => RowToPerson(Persons.SingleOrDefault(r => r.Id == id));
        public Person Update(Person person)
        {
            throw new System.NotImplementedException();
        }

        #endregion Public Methods

        #region Private Methods

        private Address RowToAddress(AddressesRow row)
        {
            Address address = null;
            if (row != null)
            {
                address = new Address()
                {
                    Id = row.Id,
                    City = row.City,
                    Country = row.Country,
                    Street = row.Street,
                    StreetNo = row.StreetNo,
                    Type = (AddressType)row.Type
                };
            }

            return address;
        }

        private Person RowToPerson(PersonsRow row)
        {
            Person person = null;
            if (row != null)
            {
                person = new Person()
                {
                    Id = row.Id,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    SecondName = row.SecondName,
                    Title = row.Title
                };

                foreach (var addressesRow in row.GetAddressesRows())
                {
                    var address = RowToAddress(addressesRow);
                    address.Person = person;
                }
            }

            return person;
        }

        #endregion Private Methods
    }
}