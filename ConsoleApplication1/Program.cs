﻿using Autofac;
using Domain.Model;
using Infrastructure.Logging;
using Infrastructure.Logging.Impl;
using Services;
using Services.Impl.NH.ContainerConfig;
using System;
using Infrastructure.Configuration;
using Infrastructure.Configuration.Impl;
using Services.Impl.Mem.AutofacIntegration;

namespace Example1
{
    internal class Program
    {
        #region Private Methods

        private static void Main(string[] args)
        {
            var builder = new ContainerBuilder();


            builder
                .RegisterType<ApplicationConfigurationXml>()
                .As<IAppConfig>()
                .SingleInstance();

            builder
                .RegisterType<ConsoleLogger>()
                .As<ILogger>()
                .SingleInstance();


            //builder.RegisterType<ConsoleConfigurableLogger>()
            //    .WithParameter(new NamedParameter("separator", '|'))
            //    .WithParameter(new NamedParameter("prefix", "MYLOGGER"))
            //    .As<ILogger>()
            //    .SingleInstance();

            //builder.RegisterType<ConsoleConfigurableLogger>()
            //    .WithParameter(new TypedParameter(typeof(char), '|'))
            //    .WithParameter(new TypedParameter(typeof(string), "MYLOGGER"))
            //    .As<ILogger>()
            //    .SingleInstance();

            //builder.Register((c,p) => new ConsoleConfigurableLogger(p.Named<char>("separator"),p.Named<string>("prefix")))
            //    .As<ILogger>()
            //    .SingleInstance();

            //builder.Register((c, p) => new ConsoleConfigurableLogger(c.Resolve<IAppConfig>().LoggerSeparator, p.Named<string>("prefix")))
            //    .As<ILogger>()
            //    .SingleInstance();

            builder
                .RegisterModule(new NHServicesConfig());
            //builder
            //  .RegisterModule(new MemServicesConfig());

            //override default registration in module
            //onactivating action
            //builder
            //    .RegisterType<PersonService>()
            //    .As<IPersonService>()
            //    .OnActivating(e => e.Context.Resolve<ILogger>().Info("Service is activating..."));

            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                var appSettings = scope.Resolve<IAppConfig>();
                var logger = scope.Resolve<ILogger>();
                //var logger = scope.Resolve<ILogger>(new NamedParameter("separator", appSettings.LoggerSeparator), new NamedParameter("prefix", "AUTOFAC"));
                //var logger = scope.Resolve<ILogger>(new NamedParameter("prefix", "AUTOFAC"));


                var personService = scope.Resolve<IPersonService>();
                
                //var nhPersonService = scope.ResolveNamed<IPersonService>("nhPersonService");
                //var memPersonService = scope.ResolveNamed<IPersonService>("memPersonService");


                
                logger.Info("Application start");
                logger.Info($"{nameof(personService)} has type: {personService.GetType().Name}");
                logger.Info($"{nameof(logger)} has type: {logger.GetType().Name}");

                logger.Info("Adding person");

                var person = new Person
                {
                    FirstName = "Damian",
                    LastName = "Langer",
                    Title = "Dev"
                };

                logger.Info("Before persisting");

                logger.Info(person.ToString());

                personService.Add(person);

                logger.Info("After persisting");

                logger.Info(person.ToString());
            }

            Console.WriteLine("Press ENTER");
            Console.ReadLine();
        }

        #endregion Private Methods
    }
}